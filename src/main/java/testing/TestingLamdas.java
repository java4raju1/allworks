package testing;

import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public class TestingLamdas {
	public static void main(String[] args) {

		List<Employee> empList = Arrays.asList(new Employee("rahul", 25, 100),
				new Employee("raju", 10, 500),
				new Employee("Mohan", 50, 200),
				new Employee("Sohan", 40, 300),
				new Employee("Gautam", 30, 800),
				new Employee("Sanjay", 20, 400));
		
		Function<Employee, String> function = e -> e.getName();
		
		Function<Employee, Integer> function2 = e -> e.getAge();
		
		  
		
		List<String> nameList = empList.stream().map(function).collect(Collectors.toList());
		
	}
}

class Employee {
	private String name;
	private int age;
	private double salary;

	public Employee(String name, int age, double salary) {
		super();
		this.name = name;
		this.age = age;
		this.salary = salary;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public double getSalary() {
		return salary;
	}

	public void setSalary(double salary) {
		this.salary = salary;
	}

}

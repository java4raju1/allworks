package java4raju.SystemDesign.ParkingLot.Model;

public enum VehicleCategory {
    TwoWheeler,
    Hatchback,
    Sedan,
    SUV,
    Bus
}

package java4raju.SystemDesign.RateLimiter;

public interface RateLimiter {
    boolean grantAccess();
}

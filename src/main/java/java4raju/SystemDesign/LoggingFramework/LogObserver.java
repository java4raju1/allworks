package java4raju.SystemDesign.LoggingFramework;

interface LogObserver {
     void log(String message);
}

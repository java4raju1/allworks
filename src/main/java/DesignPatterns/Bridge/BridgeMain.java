package DesignPatterns.Bridge;


public class BridgeMain {
    public static void main(String[] args) {
        Notification qrMessage = new QRMessage(new SMS());
        qrMessage.sendMessage();
    }
}
